(() => {
    const widget = document.querySelector('.lkdm_vw__container');
    const widgetButton = document.querySelector('.lkdm_vw__button');
    const widgetMedia = document.querySelector('.lkdm_vw__media');
    const widgetVideo = document.querySelector('.lkdm_vw__video');
    const widgetMuteButton = document.querySelector('.lkdm_vw__mute');
    const widgetProgressBar = document.querySelector('.lkdm_vw__progressbar');
    const widgetProgress = document.querySelector('.lkdm_vw__progress');
    const widgetTime = document.querySelector('.lkdm_vw__time');
    const widgetTimeSeconds = document.querySelector('.lkdm_vw__time--seconds');
    const widgetStop = document.querySelector('.lkdm_vw__stop');
    const widgetPlay = document.querySelector('.lkdm_vw__play');

    let widgetIsMobile = {
        Android: function () {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function () {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function () {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function () {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function () {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function () {
            return (widgetIsMobile.Android() || widgetIsMobile.BlackBerry() || widgetIsMobile.iOS() || widgetIsMobile.Opera() || widgetIsMobile.Windows());
        }
    };

    let lkdm_vw_options_position = 'left';
    //    left - виджет расположен слева
    //    right - виджет расположен справа

    let lkdm_vw_options_mob_invert = 0;
    //    0 - инверсия на мобильынх отключена
    //    1 - инверсия на мобильных включена (если виджет на ПК-версии слева, то на мобильной - справа, если виджет на ПК-версии справа, то на мобильной - слева)


    let lkdm_vw_options_mob_show = 0;
    //    0 - не показывать виджет на мобильных
    //    1 - показывать виджет всегда

    let lkdm_vw_options_btn = 1;
    //    0 - не показывать CTA-кнопку в виджете
    //    1 - показывать

    //    Цвет CTA-кнопки
    let lkdm_vw_options_btn_color = 'rgba(255,177,61,1)';

    //    Цвет текста в CTA-кнопке
    let lkdm_vw_options_btn_link_color = '#ffffff';

    let lkdm_vw_options_progressbar = 1;

    let lkdm_vw_options_border = 1;

    let lkdm_vw_options_preview_text_enabled = 1;

    let lkdm_vw_options_preview_text = 'Очень интересный текст, Очень интересный текст';

    if (widget) {

        if (lkdm_vw_options_position == 'right') {
            widget.classList.add('lkdm_vw__container--right')
        }

        if(lkdm_vw_options_border) {
            let widgetBorder = document.createElement('div');
            widgetBorder.classList.add('lkdm_vw__border');
            widgetMedia.appendChild(widgetBorder);
            widget.classList.add('lkdm_vw__container--border');
            widgetBorder.style.borderColor = lkdm_vw_options_btn_color;
            widgetProgressBar.style.top = '4px';
        }

        if(lkdm_vw_options_btn_color) {
            widgetButton.style.background = lkdm_vw_options_btn_color;
        }

        if(lkdm_vw_options_btn_link_color) {
            widgetButton.style.color = lkdm_vw_options_btn_link_color;
        }

        if(!lkdm_vw_options_btn) {
            widgetButton.remove();
        }

        if(lkdm_vw_options_mob_invert) {
            widget.classList.add('lkdm_vw__container--inverse');
        }

        if(!lkdm_vw_options_mob_show) {

            if (widgetIsMobile.any()) {
                widget.remove();
            };
        }

        if (lkdm_vw_options_preview_text_enabled) {
            let widgetText = document.createElement('div');
            widgetText.classList.add('lkdm_vw__text');
            widgetText.textContent = lkdm_vw_options_preview_text;
            widgetMedia.appendChild(widgetText);
        }

        if (lkdm_vw_options_progressbar) {
            widgetVideo.addEventListener('loadedmetadata', function() {
                let videoPercent = 100 / Math.round(widgetVideo.duration);
                console.log(videoPercent);

                function progressLoop() {
                    setInterval(function () {
                        let videoPosition = Math.round(widgetVideo.currentTime * videoPercent) + '%';
                        widgetTimeSeconds.innerHTML = Math.round(widgetVideo.currentTime).toString().padStart(2, '0');
                        widgetProgress.style.width = videoPosition;
                    });
                }
                progressLoop();
            });
        }
        else {
            widgetProgressBar.remove();
            widgetTime.remove();
        }

        document.addEventListener('click', lkdmWidget);

        function lkdmWidget(event){

            if (event.target.closest('.lkdm_vw__media')) {
                if(widget.classList.contains('lkdm_vw__container--open')) {
                    if (widgetVideo.paused) {
                        widgetVideo.play();
                        widgetStop.classList.remove('active');
                        widgetPlay.classList.add('active');
                    } else {
                        widgetVideo.pause();
                        widgetStop.classList.add('active');
                        widgetPlay.classList.remove('active');
                    }
                }
                else {
                    widget.classList.add('lkdm_vw__container--open');
                    widget.classList.add('lkdm_vw__container--active');
                    widgetVideo.currentTime = 0;
                    widgetVideo.muted = false;
                }
            }
            if (event.target.closest('[data-lkdm-close]')) {
                let isOpen = document.querySelector('.lkdm_vw__container--open');
                if (isOpen) {
                    widget.classList.remove('lkdm_vw__container--open');
                    widget.classList.remove('lkdm_vw__container--active');
                    widgetVideo.play();
                    widgetVideo.muted = true;
                }
                else {
                    widget.classList.add('lkdm_vw__container--hide');
                }
            }
            if (event.target.closest('[data-lkdm-mute]')){
                widgetVideo.muted = !widgetVideo.muted;
                widgetMuteButton.classList.toggle('lkdm_vw__mute--off')
            }

            if (event.target.closest('.lkdm_vw__container')) {

            }
            else {
                widget.classList.remove('lkdm_vw__container--open');
                widget.classList.remove('lkdm_vw__container--active');
                widgetVideo.play();
                widgetVideo.muted = true;
            }
        }
    }

})();
